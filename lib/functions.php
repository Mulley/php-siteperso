<?php

/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent(){
	if(!isset($_GET['page'])){
		include __DIR__.'/../pages/home.php';
		include __DIR__.'/../pages/bio.php';
		include __DIR__.'/../pages/contact.php';
	} else {
		// le reste du code
	}
}

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}

function getUserData(){
	var_export (json_decode(file_get_contents('../data/user.json')));
}
